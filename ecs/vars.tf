variable "service_type" {
    type = string
    default = "admin"
    description =  "service"
}

variable "region"{
    type        = string
    description = "the name of the region you wish to deploy into"
    default     = "ap-south-1"
}
