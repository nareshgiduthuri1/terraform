terraform {
  required_version = ">= 0.13"
  
  # backend "remote" {}
}

provider "aws" {
  region = "ap-south-1"
}

module "admin" {
    source = "../ecs"
    #version                = "~> 2.0"
    service_type = "admin"
    region = "ap-south-1"
    #execution_role_arn = "${aws_iam_role.ECSTaskExecutionRole.arn}"
    #cluster_id = aws_ecs_cluster.ECS_Fargate.id
    #cluster_name = aws_ecs_cluster.ECS_Fargate.name
    #desired_count = 1
    #vpc_id = "${aws_vpc.my_vpc.id}"
    #subnets = ["${aws_subnet.my_subnet.id}"]
    #security_groups = ["${aws_security_group.my_sg.id}"]
}

resource "aws_route53_zone" "myzone" {
  name = "myzone.com"

  tags = {
    Environment = "dev"
  }
}

resource "aws_route53_record" "adminroute" {
  zone_id = aws_route53_zone.myzone.id
  name    = "adminroute"
  type    = "A"
  ttl     = "300"
  #records = [module.admin.private_ip]
  records = ["${aws_instance.terraform_instance.private_ip}"]
  depends_on = [
    module.admin
  ]
# #records = ["10.111.123.234"]
}

resource "aws_route53_record" "adminroute" {
  zone_id = aws_route53_zone.myzone.id
  name    = "adminroute"
  type    = "A"
  ttl     = "300"
  records = ["${module.admin.aws_ecs_task_definition.ECS_task.private_ip}"]
  depends_on = [
    module.admin
  ]
  #records = ["10.111.123.234"]
}