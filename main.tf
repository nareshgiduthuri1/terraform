resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "my_vpc"
  }
}

resource "aws_internet_gateway" "my_internetgateway" {
  vpc_id = "${aws_vpc.my_vpc.id}"
  tags = {
      Name = "my_internetgateway"
  }
}

resource "aws_route_table" "my_routetable" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    tags = {
        Name = "my_routetable"
        }
}

resource "aws_route" "my_route" {
    route_table_id = "${aws_route_table.my_routetable.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_internetgateway.id}"
    #nat_gateway_id = "${aws_nat_gateway.my_gateway.id}"
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.my_subnet.id}"
  route_table_id = "${aws_route_table.my_routetable.id}"
}

resource "aws_subnet" "my_subnet" {
    #count = "2"
    vpc_id = "${aws_vpc.my_vpc.id}"
    cidr_block = "${cidrsubnet(aws_vpc.my_vpc.cidr_block, 3, 1)}"
    availability_zone = "ap-south-1a"
}

resource "aws_subnet" "my_subnet1" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    cidr_block = "${cidrsubnet(aws_vpc.my_vpc.cidr_block, 3, 2)}"
    availability_zone = "ap-south-1b"
}

#resource "aws_eip" "my_eip" {
#  instance = "${aws_instance.terraform_instance_kafka.id}"
#  vpc      = true
#  depends_on = [
#    aws_internet_gateway.my_internetgateway
#  ]
#}
#
resource "aws_eip" "my_eip1" {
  instance = "${aws_instance.terraform_instance_smile.id}"
  vpc      = true
  depends_on = [
    aws_internet_gateway.my_internetgateway
  ]
}

#resource "aws_nat_gateway" "my_gateway" {
#  subnet_id     = aws_subnet.my_subnet.id
#  allocation_id = aws_eip.my_eip.id
#
#  tags = {
#    Name =  "my_nat_gateway"
#  }
#}



resource "aws_security_group" "my_sg" {
    name = "my_security_group"
    vpc_id = "${aws_vpc.my_vpc.id}"

   ingress  {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}



resource "aws_instance" "terraform_instance_kafka" {
    ami = "ami-0a23ccb2cdd9286bb"
    iam_instance_profile = "iam_fullaccess"
    instance_type = "t2.micro"
    key_name = "terraform"
    security_groups = ["${aws_security_group.my_sg.id}"]
      # root disk
    #root_block_device {
    #  volume_size           = "30"
    #  volume_type           = "gp2"
    #  #encrypted             = true
    #  #kms_key_id            = aws_kms_key.kopi-kms-key.key_id      
    #  delete_on_termination = true
    #}
    ## data disk
    #ebs_block_device {
    #  device_name           = "/dev/xvda"
    #  volume_size           = "30"
    #  volume_type           = "gp2"
    #  #encrypted             = true
    #  #kms_key_id            = aws_kms_key.kopi-kms-key.key_id    
    #  delete_on_termination = true
  #}
    #user_data = <<-EOF
    #          ${file("/Scripts/install_java.sh")}
    #          chmod +x install_java.sh 
    #          ./install_java.sh
    #          EOF

    tags = {
        Name = "terraform_instance_kafka"
    }
    subnet_id = "${aws_subnet.my_subnet.id}"
}
resource "aws_instance" "terraform_instance_smile" {
    ami                   = "ami-0a23ccb2cdd9286bb"
    iam_instance_profile  = "iam_fullaccess"
    instance_type         = "t2.micro"
    key_name              = "terraform"
    security_groups       = ["${aws_security_group.my_sg.id}"]
    #user_data             = data.template_cloudinit_config.config.rendered
    user_data             = <<-EOF
                             ${data.template_cloudinit_config.config.rendered}
                             chmod +x install_java.sh
                             ./install_java.sh >> /tmp/install.log
                             EOF
    tags = {
        Name = "terraform_instance_smile"
    }
    subnet_id = "${aws_subnet.my_subnet.id}"
}
data "template_file" "client" {
  template = file("./Scripts/install_java.sh")

  vars = {
    #instance_id   = "${aws_instance.terraform_instance_smile.id}"
    #kafka_ip      = "${aws_instance.terraform_instance_kafka.private_ip}"
    #rds_endpoint  = "${aws_rds_cluster.rds_cluster.endpoint}"
  }
}

data "template_cloudinit_config" "config" {
  gzip          = false
  base64_encode = false
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.client.rendered}"
  }
}

resource "aws_instance" "terraform_instance_smile" {
    ami = "ami-00bf4ae5a7909786c"
    iam_instance_profile = "iam_fullaccess"
    instance_type = "t2.micro"
    key_name = "terraform"
    security_groups = ["${aws_security_group.my_sg.id}"]
    #user_data             = data.template_cloudinit_config.config.rendered
    #user_data = <<-EOF
    #          ${file("/Scripts/install_java.sh")}
    #          chmod +x install_java.sh "${aws_instance.terraform_instance_kafka.private_ip}" "${aws_rds_cluster.rds_cluster.endpoint}"
    #          ./install_java.sh >> /tmp/install.log
    #          EOF

    tags = {
        Name = "terraform_instance_smile"
    }
    subnet_id = "${aws_subnet.my_subnet.id}"
    #depends_on = [
    #  aws_instance.terraform_instance_kafka
    #  aws_rds_cluster.rds_cluster
    #]
    #instance_initiated_shutdown_behavior  =  "terminate"
    timeouts {
      delete = "5m"
  }
}

data "aws_secretsmanager_secret" "password" {
  name = "ash/creds"
}

data "aws_secretsmanager_secret_version" "creds" {
  secret_id = "${data.aws_secretsmanager_secret.password.id}"
}

#locals {
#  your_secret = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)
#}

resource "aws_rds_cluster_parameter_group" "rds_paramgroup" {
  name        = "postgres-paramgroup"
  family      = "aurora-postgresql12"
  description = "RDS Postgres Smile CDR cluster parameter group"
}

resource "aws_rds_cluster" "rds_cluster" {
  cluster_identifier              = "my-rds-cluster"
  engine                          = "aurora-postgresql"
  engine_version                  = "12.4"
  availability_zones              = ["ap-south-1a"]
  # database_name                   = "smilecdr"
  #master_username                 = local.your_secret.username
  master_username                 = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)["username"]
  #master_password                 = local.your_secret.password
  master_password                 = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)["password"]
  backup_retention_period         = 30
  preferred_backup_window         = "21:00-23:00"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.rds_paramgroup.name}"
  #vpc_security_group_ids          = ["sg-03d74ac38f9cf3817"]
  #db_subnet_group_name            = "cobalt-aurora-db-subnet-group"
  skip_final_snapshot             = true
  final_snapshot_identifier       = "hdc-core-smilecdr-final-snapshot"
}
resource "aws_msk_configuration" "mks_config_scram" {
  name = "demo-mks-config-scram"
  #kafka_versions = ["2.8.0"]
  server_properties = <<PROPERTIES
auto.create.topics.enable = true
delete.topic.enable = true
PROPERTIES
}

resource "aws_cloudwatch_log_group" "logs" {
  name = "Yada"

  tags = {
    Environment = "production"
  }
}
resource "aws_msk_cluster" "msk_cluster" {
  cluster_name           = "mskcluster"
  kafka_version          = "2.8.0"
  number_of_broker_nodes = 2
  configuration_info {
    arn      = aws_msk_configuration.mks_config_scram.arn
    revision = 1
  }

  broker_node_group_info {
    instance_type   = "kafka.t3.small"
    # ebs_volume_size = "1"
    client_subnets = [
      aws_subnet.my_subnet.id,
      aws_subnet.my_subnet1.id
    ]
    security_groups = [
      aws_security_group.my_sg.id
    ]
  }

  encryption_info {
    encryption_in_transit {
      client_broker = "TLS_PLAINTEXT"
      in_cluster    = true
    }
    encryption_at_rest_kms_key_arn = "arn:aws:kms:ap-south-1:462703886534:key/1828830d-b443-403e-991c-28bc969e3677"
  }
  
  client_authentication {
    unauthenticated = {
      enabled = true
    }
    sasl {
      iam = true
    }
  }

  #open_monitoring {
  #  prometheus {
  #    jmx_exporter {
  #      enabled_in_broker = true
  #    }
  #    node_exporter {
  #      enabled_in_broker = true
  #    }
  #  }
  #}

  #logging_info {
  #  broker_logs {
  #    cloudwatch_logs {
  #      enabled   = true
  #      log_group = aws_cloudwatch_log_group.logs.name
  #    }
  #  }
  #}

  tags = {
    Name = "Amazon MSK Demo Cluster SASL/SCRAM"
  }
}
