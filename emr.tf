terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}

resource "aws_emr_cluster" "cluster" {
  name          = "Terraform_Testing_EMR_Creation"
  release_label = "emr-6.2.1"
  applications  = ["Spark"]
  service_role = "Cobalt-EmrDefault"
  termination_protection            = false
  keep_job_flow_alive_when_no_steps = true

  ec2_attributes {
    subnet_id                         = "subnet-83db69ae"
    emr_managed_master_security_group = "sg-4dac2a30"
    emr_managed_slave_security_group  = "sg-4cac2a31"
    instance_profile                  = "Cobalt-EmrEc2Default"
    key_name = "IHDP-Core-Dev"
  }

  master_instance_group {
    instance_type = "c5.4xlarge"
  }

  core_instance_group {
    instance_type  = "c5.4xlarge"
    instance_count = 1
    ebs_config {
      size                 = "40"
      type                 = "gp2"
      volumes_per_instance = 1
    }
   }
   ebs_root_volume_size = 100
   tags = {
      APP_ID = "1905"
      env  = "Dev"
      Billing = "Dataplatform/DataIngestion"
      COST_CENTER="30025"
    }

  bootstrap_action {
    path = "s3://enterprise-datalake-development-us-east-1/IHDP-Core/Normalization/BootstrapScripts/AWS_Set_Region.sh"
    name = "bootstrap-set-region"
    #args = ["instance.isMaster=true", "echo running on master node"]
  }

  configurations_json = <<EOF
  [
    {
      "Classification": "hadoop-env",
      "Configurations": [
        {
          "Classification": "export",
          "Properties": {
            "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
          }
        }
      ],
      "Properties": {}
    },
    {
      "Classification": "spark-env",
      "Configurations": [
        {
          "Classification": "export",
          "Properties": {
            "JAVA_HOME": "/usr/lib/jvm/java-1.8.0"
          }
        }
      ],
      "Properties": {}
    }
  ]
EOF
}

#SNS Topics
resource "aws_sns_topic" "awssnstopic" {
  name = "testsns"
}

#SQS Deadlater Queue
resource "aws_sqs_queue" "deadlater" {
  name = "deadlaterqueue"
}

#SQS Primary

resource "aws_sqs_queue" "awssqsqueue" {
  name = "primarysqs"
  redrive_policy = "{\"deadLetterTargetArn\" : \"${aws_sqs_queue.deadlater.arn}\",\"maxReceiveCount\":5}"
  visibility_timeout_seconds = 300
  tags = {
    "enviornment" = "dev"
  }
}

#SNS Toipc Subscription
resource "aws_sns_topic_subscription" "snstopicsubscription" {
  topic_arn = aws_sns_topic.awssnstopic.arn
  protocol = "sqs"
  endpoint = aws_sqs_queue.awssqsqueue.arn
}

#SQS Policy

resource "aws_sqs_queue_policy" "sqsqueuepolicy" {

  queue_url = aws_sqs_queue.awssqsqueue.id
  policy = <<POLICY
    {
      version="2022-04-06",
      id="sqspolicy",
      Statement:
      [
          {
        "Sid":"First",
        "Effect":"Allow",
        "Principle":"*",
        "Action":"sqs.sendMessage",
        "Resource": "${aws_sqs_queue.awssqsqueue.arn}",
        "Condition":{
              "ArnEquals":{
                    "aws.SourceArn" : "${aws_sns_topic.awssnstopic.arn}"
                          }
                    }
          }
      ]
    }
POLICY
}