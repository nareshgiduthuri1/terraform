resource "aws_rds_cluster_parameter_group" "rds_paramgroup" {
  name        = "postgres-paramgroup"
  family      = "aurora-postgresql12"
  description = "RDS Postgres Smile CDR cluster parameter group"
}

resource "aws_rds_cluster" "rds_cluster" {
  cluster_identifier              = "my-rds-cluster"
  engine                          = "aurora-postgresql"
  engine_version                  = "12.4"
  availability_zones              = ["ap-south-1a"]
  # database_name                   = "smilecdr"
  master_username                 = "smile"
  master_password                 = "InteropCHC!"
  backup_retention_period         = 30
  preferred_backup_window         = "21:00-23:00"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.rds_paramgroup.name}"
  #vpc_security_group_ids          = ["sg-03d74ac38f9cf3817"]
  #db_subnet_group_name            = "cobalt-aurora-db-subnet-group"
  skip_final_snapshot             = true
  final_snapshot_identifier       = "hdc-core-smilecdr-final-snapshot"

}

resource "aws_rds_cluster_instance" "rds_instances" {
  count                         = 1
  identifier                    = "hdc-core-smilecdr-postgres-instances"
  cluster_identifier            = aws_rds_cluster.rds_cluster.id
  instance_class                = "db.r4.large"
  engine                        = aws_rds_cluster.rds_cluster.engine
  engine_version                = aws_rds_cluster.rds_cluster.engine_version
  performance_insights_enabled  = true
}

resource "aws_rds_cluster_instance" "rds_reader" {
  count                         = 1
  identifier                    = "hdc-core-smilecdr-postgres-reader"
  cluster_identifier            = aws_rds_cluster.rds_cluster.id
  instance_class                = "db.r4.large"
  engine                        = aws_rds_cluster.rds_cluster.engine
  engine_version                = aws_rds_cluster.rds_cluster.engine_version
  performance_insights_enabled  = true
}