#!/bin/bash

echo "Deployment with terraform starts"
echo "building docker images"
ecr_repo=462703886534.dkr.ecr.ap-south-1.amazonaws.com
image=terraform_ecs_docker
service=admin
now=$(date +"%T")

echo "building docker image for admin"
image_tag=${image}:${service}
echo ${image_tag}
image_uri=${ecr_repo}/${image_tag}
echo ${image_uri}
aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin ${ecr_repo}
#aws ecr create-repository --repository-name test
docker build -t test ./docker/
docker tag test:latest ${ecr_repo}/test:latest
docker push ${ecr_repo}/test:latest

#docker build --build-arg service=admin -t ${image_tag} ./docker/
#docker tag ${image_tag} ${ecr_repo}/${image_tag}
#docker push ${ecr_repo}/${image_tag}
#aws ssm put-parameter --name "/docker/${admin}" --value "${image_uri}" --overwrite --type String --region ap-south-1
#aws ssm put-parameter --cli-input-json '{"Name": "/docker/${admin}", "Value": "${image_uri}", "Type": "String"}'


#terraform init
#terraform plan
#terraform apply -auto-approve

echo "Deployment complete"



