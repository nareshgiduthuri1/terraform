#! /bin/bash
sudo su
sudo yum update -y
sudo yum install -y java
sudo yum install jq
ip_test=${kafka_ip}
log $ip_test
cd /home/ec2-user/
echo "ip - ${kafka_ip}"
aws s3 cp s3://awsathena-demo/employee_json.json . --region ap-south-1
sed -i "s/{{ip}}/${kafka_ip}/g" employee_json.json
mkdir files

echo "setting kafka ip to hosts file"
formatted_ip=`echo ${kafka_ip} | sed s/[.]/-/g`
log ${formatted_ip}
emdeon_ip=`ip-${formatted_ip}-emdeon.net`
log ${emdeon_ip}
echo "${kafka_ip}" "${formatted_ip}" "${emdeon_ip}" >> /etc/hosts
#chmod +x employee_json.json

