resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "my_vpc"
  }
}

resource "aws_internet_gateway" "my_internetgateway" {
  vpc_id = "${aws_vpc.my_vpc.id}"
  tags = {
      Name = "my_internetgateway"
  }
}

resource "aws_route_table" "my_routetable" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    tags = {
        Name = "my_routetable"
        }
}

resource "aws_route" "my_route" {
    route_table_id = "${aws_route_table.my_routetable.id}"
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_internetgateway.id}"
    #nat_gateway_id = "${aws_nat_gateway.my_gateway.id}"
}

resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.my_subnet.id}"
  route_table_id = "${aws_route_table.my_routetable.id}"
}

resource "aws_subnet" "my_subnet" {
    vpc_id = "${aws_vpc.my_vpc.id}"
    cidr_block = "${cidrsubnet(aws_vpc.my_vpc.cidr_block, 3, 1)}"
    availability_zone = "ap-south-1a"
}

#resource "aws_eip" "my_eip" {
#  instance = "${aws_instance.terraform_instance.id}"
#  vpc      = true
#  depends_on = [
#    aws_internet_gateway.my_internetgateway
#  ]
#}

#resource "aws_eip" "my_eip1" {
#  vpc      = true
#  depends_on = [
#    aws_internet_gateway.my_internetgateway
#  ]
#}
#
#resource "aws_nat_gateway" "my_gateway" {
#  subnet_id     = aws_subnet.my_subnet.id
#  allocation_id = aws_eip.my_eip.id
#
#  tags = {
#    Name =  "my_nat_gateway"
#  }
#}

#resource "aws_iam_role" "ECSTaskExecutionRole" {
#  name = "iam_role_ecs"
#
#  # Terraform's "jsonencode" function converts a
#  # Terraform expression result to valid JSON syntax.
#  assume_role_policy = jsonencode({
#    Version = "2012-10-17"
#    Statement = [
#      {
#        Action = "sts:AssumeRole"
#        Effect = "Allow"
#        Principal = {
#          Service = "ecs-tasks.amazonaws.com"
#        }
#      },
#    ]
#  })
#
#   tags = {
#    Environment = "aws-quickstarts-fargate"
#  }
#}

#data "aws_iam_policy_document" "ecs_task_exec_role" {
#  statement {
#    actions = ["sts:AssumeRole"]
#
#    principals {
#      type        = "Service"
#      identifiers = ["ecs-tasks.amazonaws.com"]
#    }
#  }
#}
#
#resource "aws_iam_role" "ecsTaskExecutionRole" {
#  name               = "taskrole-ecs"
#  assume_role_policy = data.aws_iam_policy_document.ecs_task_exec_role.json
#}
#
#resource "aws_iam_role_policy_attachment" "ecs_task_exec_role" {
#  role       = aws_iam_role.ecsTaskExecutionRole.name
#  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
#}

resource "aws_security_group" "my_sg" {
    name = "my_security_group"
    vpc_id = "${aws_vpc.my_vpc.id}"

   ingress  {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_cluster" "ECS_Fargate" {
  name = "ecs_fargate_cluster"
  #capacity_providers = []

  setting {
    name = "containerInsights"
    value = "enabled"
  }
}

resource "aws_iam_role" "ECSTaskExecutionRole" {
  name = "iam_role_ecs"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

   tags = {
    Environment = "aws-quickstarts-fargate"
  }
}

resource "aws_ecs_task_definition" "ECS_task" {
  family = "smile"
  cpu = 256
  memory =512
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn = "${aws_iam_role.ECSTaskExecutionRole.arn}"
  container_definitions = jsonencode([
  {
    "cpu": 256
    "image": "462703886534.dkr.ecr.ap-south-1.amazonaws.com/test:latest"
    "memory": 512
    "name": "smile"
    "portMappings": [
      {
        "containerPort": 80
      }
    ]
  }
])
}

resource "aws_ecs_service" "ECS_service" {
 #depends_on = [aws_lb.public]
  name          = "ecs-service_admin"
  cluster       = aws_ecs_cluster.ECS_Fargate.id
  task_definition = aws_ecs_task_definition.ECS_task.arn
  launch_type = "FARGATE"
  deployment_maximum_percent = "200"
  deployment_minimum_healthy_percent  = "75"
  desired_count = 1
  network_configuration {
    subnets = ["${aws_subnet.my_subnet.id}"]
    assign_public_ip = false
    security_groups = ["${aws_security_group.my_sg.id}"]
  }
}

resource "aws_instance" "terraform_instance" {
    ami = "ami-00bf4ae5a7909786c"
    instance_type = "t2.micro"
    key_name = "terraform"
    security_groups = ["${aws_security_group.my_sg.id}"]
    user_data = <<-EOF
              ${file("/Scripts/install_java.sh")}
              chmod +x install_java.sh
              ./install_java.sh
              EOF

    tags = {
        Name = "terraform_instance"
    }
    subnet_id = "${aws_subnet.my_subnet.id}"
}

resource "aws_route53_zone" "myzone" {
  name = "myzone.com"

  tags = {
    Environment = "dev"
  }
}
resource "aws_route53_record" "adminroute" {
  zone_id = aws_route53_zone.myzone.id
  name    = "adminroute"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.terraform_instance.private_ip}"]
  #depends_on = [
  #  module.admin
  #]
  #records = ["10.111.123.234"]
}