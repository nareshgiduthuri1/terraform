aws ecr describe-repositories --query "repositories[repositoryName, `my_repository`].[repositoryName,repositoryUri]" --output text --no-cli-pager

aws ecr describe-repositories --query "repositories[].[my_repository]" --output text --no-cli-pager

aws ecr describe-repositories --repository-name my_repository --query "repositories[].repositoryName" --output text --region us-east-1 

aws ecr describe-repositories --query 'repositories[?starts_with(repositoryName, `my_repository`) == `true`]|[].[repositoryName,repositoryUri]'

aws ec2 terminate-instances --instance-ids ${instance_id}